package main

/*
Дана строка s, найдите длину самой длинной
подстроки без повторяющихся символов.
*/

func Sliding05(s string) int {
	rns := []rune(s)

	cnt := 0

	win1 := 0
	win2 := 0

	mp := make(map[rune]struct{})

	for i := range rns {
		win1 = i

		f := func(r rune) bool {
			_, ok := mp[rns[i]]

			return ok
		}

		for f(rns[i]) {
			delete(mp, rns[win2])

			win2++
		}

		mp[rns[i]] = struct{}{}

		if cnt < win1+1-win2 {
			cnt = win1 + 1 - win2
		}
	}

	return cnt
}
