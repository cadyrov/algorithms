package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSliding03(t *testing.T) {
	assert.Equal(t, "bdbdbbd", Sliding03("abcbdbdbbdcdabd", 2))
	assert.Equal(t, "bcbdbdbbdcd", Sliding03("abcbdbdbbdcdabd", 3))
	assert.Equal(t, "abcbdbdbbdcdabd", Sliding03("abcbdbdbbdcdabd", 5))
}
