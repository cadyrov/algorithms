package main

/*
Дана строка и положительное число k,
найти самую длинную подстроку строки, содержащей k различных символов.
Если k больше, чем общее количество различных символов в строке, вернуть всю строку.
*/

func Sliding03(in string, k int) string {
	if k > len(in) {
		return in
	}

	rns := []rune(in)

	var subStart, subEnd int //границы самой длинной подстроки

	vc := make(map[rune]int) // словарь символов

	var winStart, winEnd int // границы скользящего окна

	for i := 0; i < len(rns); i++ {
		winEnd = i + 1
		//проверить есть ли символ в словаре
		if _, ok := vc[rns[i]]; !ok {
			vc[rns[i]] = 1
		} else {
			vc[rns[i]] += 1
		}

		for len(vc) > k {
			if v, _ := vc[rns[winStart]]; v == 1 {
				delete(vc, rns[winStart])
			} else {
				vc[rns[winStart]] -= 1
			}

			winStart += 1
		}

		if winEnd-winStart > subEnd-subStart {
			subEnd, subStart = winEnd, winStart
		}
	}

	return string(rns[subStart:subEnd])
}
