package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSliding01(t *testing.T) {
	assert.Equal(t, 12, Sliding01([]int{5, 2, 5, 4, 3, 2}, 3))
}
