package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSliding02(t *testing.T) {
	assert.Equal(t, []int{5}, Sliding02([]int{1, 1, 3, 1, 5, 2, 2, 3}, 5))
	assert.Equal(t, []int{5, 2}, Sliding02([]int{1, 3, 3, 1, 5, 2, 2, 3}, 7))
}
