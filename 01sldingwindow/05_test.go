package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSliding05(t *testing.T) {
	assert.Equal(t, 3, Sliding05("abcabcbb"))
	assert.Equal(t, 1, Sliding05("bbbbb"))
	assert.Equal(t, 3, Sliding05("pwwkew"))
}
