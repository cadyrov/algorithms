package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSliding04(t *testing.T) {
	assert.Equal(t, 3, Sliding04([]int{1, 2, 1}))
	assert.Equal(t, 3, Sliding04([]int{0, 1, 2, 2}))
	assert.Equal(t, 4, Sliding04([]int{1, 2, 3, 2, 2}))
}
