package main

/*
Вы посещаете ферму с одним рядом фруктовых деревьев, расположенных слева направо.
Деревья представлены целочисленным массивом фруктов, где фрукты[i] — это тип фруктов, которые производит i-е дерево.

Вы хотите собрать как можно больше фруктов.
Однако у владельца есть несколько строгих правил, которым необходимо следовать:

У вас есть только две корзины, и каждая корзина может содержать только один вид фруктов.
Количество фруктов в каждой корзине не ограничено.
Начиная с любого дерева по вашему выбору,
вы должны собрать ровно по одному фрукту с каждого дерева (включая стартовое дерево), двигаясь вправо.
Собранные фрукты должны поместиться в одну из ваших корзин.
Как только вы доберетесь до дерева с фруктами, которые не помещаются в ваши корзины, вы должны остановиться.
Учитывая целочисленный массив фруктов, верните максимальное количество фруктов, которые вы можете собрать.
*/

func Sliding04(in []int) int {
	basketWindowFirstIdx, basketWindowSecondIdx := 0, 0

	count := 0

	baskets := make(map[int]int)

	for i := 0; i < len(in); i++ {
		basketWindowFirstIdx = i

		if _, ok := baskets[in[basketWindowFirstIdx]]; !ok {
			baskets[in[basketWindowFirstIdx]] = 1
		} else {
			baskets[in[basketWindowFirstIdx]] += 1
		}

		for len(baskets) > 2 {
			if baskets[in[basketWindowSecondIdx]] == 1 {
				delete(baskets, in[basketWindowSecondIdx])
			} else {
				baskets[in[basketWindowFirstIdx]] -= 1
			}

			basketWindowFirstIdx += 1
		}

		sm := sum(baskets)

		if sm > count {
			count = sm
		}
	}

	return count
}

func sum(mp map[int]int) int {
	sum := 0
	for _, v := range mp {
		sum += v
	}

	return sum
}