package main

/*
Наименьший подмассив с заданной суммой
на вход подается массив и целевая сумма на выход вернуть наименьший подмассив
*/

func Sliding02(in []int, sum int) []int {
	start, stop, midSum := 0, 0, in[0]
	if midSum == sum {
		return in[0:1]
	}

	curStart, curStop := 0, 1

	for i := curStop; i < len(in); i++ {
		if stop-start == 1 {
			break
		}

		midSum += in[i]
		curStop = i + 1

		for midSum > sum && curStart <= i {
			midSum -= in[curStart]

			curStart += 1
		}

		if midSum == sum && ((stop-start > curStop-curStart) || (stop == 0 && start == 0)) {
			stop, start = curStop, curStart
		}
	}

	return in[start:stop]
}
