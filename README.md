### 1. Pattern: Sliding Window

1. Максимальная сумма подмассива размера К
2. Наименьший подмассив с заданной суммой
3. Самая длинная подстрока с K различными символами
4. Фрукты в корзинке
5. Самая длинная подстрока с непоаторяющимися символами[LeetCode](https://leetcode.com/problems/longest-substring-without-repeating-characters/)
6. Longest Substring with Same Letters after Replacement  [LeetCode](https://leetcode.com/problems/longest-repeating-character-replacement/)
7. Longest Subarray with Ones after Replacement  [LeetCode](https://leetcode.com/problems/max-consecutive-ones-iii/)
8. Problem Challenge 1 - Permutation in a String  [Leetcode](https://leetcode.com/problems/permutation-in-string/)
9. Problem Challenge 2 - String Anagrams  [Leetcode](https://leetcode.com/problems/find-all-anagrams-in-a-string/)
10. Problem Challenge 3 - Smallest Window containing Substring  [Leetcode](https://leetcode.com/problems/minimum-window-substring/)
11. Problem Challenge 4 - Words Concatenation  [Leetcode](https://leetcode.com/problems/substring-with-concatenation-of-all-words/)

### 2. Pattern: Two Pointers

1. Introduction
2. Pair with Target Sum  [LeetCode](https://leetcode.com/problems/two-sum/)
3. Remove Duplicates  [LeetCode](https://leetcode.com/problems/remove-duplicates-from-sorted-list/) [LeetCode](https://leetcode.com/problems/remove-duplicates-from-sorted-list-ii/) [LeetCode](https://leetcode.com/problems/remove-duplicates-from-sorted-array-ii/) [LeetCode](https://leetcode.com/problems/find-the-duplicate-number/) [LeetCode](https://leetcode.com/problems/duplicate-zeros/)
4. Squaring a Sorted Array  [LeetCode](https://leetcode.com/problems/squares-of-a-sorted-array/)
5. Triplet Sum to Zero [LeetCode](https://leetcode.com/problems/3sum/)
6. Triplet Sum Close to Target [LeetCode](https://leetcode.com/problems/3sum-closest/)
7. Triplets with Smaller Sum [LintCode](https://www.lintcode.com/problem/3sum-smaller/description)
8. Subarrays with Product Less than a Target [LeetCode](https://leetcode.com/problems/subarray-product-less-than-k/)
9. Dutch National Flag Problem [CoderByte](https://coderbyte.com/algorithm/dutch-national-flag-sorting-problem)
10. Problem Challenge 1 - Quadruple Sum to Target [Leetcode](https://leetcode.com/problems/4sum/)
11. Problem Challenge 2 - Comparing Strings containing Backspaces [Leetcode](https://leetcode.com/problems/backspace-string-compare/)
12. Problem Challenge 3 - Minimum Window Sort [Leetcode](https://leetcode.com/problems/shortest-unsorted-continuous-subarray/) [Ideserve](https://www.ideserve.co.in/learn/minimum-length-subarray-sorting-which-results-in-sorted-array)

### 3. Pattern: Fast & Slow pointers

1. Introduction [emre.me](https://emre.me/coding-patterns/fast-slow-pointers/)
2. LinkedList Cycle  [Leetcode](https://leetcode.com/problems/linked-list-cycle/)
3. Start of LinkedList Cycle [Leetcode](https://leetcode.com/problems/linked-list-cycle-ii/)
4. Happy Number [Leetcode](https://leetcode.com/problems/happy-number/)
5. Middle of the LinkedList  [Leetcode](https://leetcode.com/problems/middle-of-the-linked-list/)
6. Problem Challenge 1 - Palindrome LinkedList [Leetcode](https://leetcode.com/problems/palindrome-linked-list/)
7. Problem Challenge 2 - Rearrange a LinkedList [Leetcode](https://leetcode.com/problems/reorder-list/)
8. Problem Challenge 3 - Cycle in a Circular Array  [Leetcode](https://leetcode.com/problems/circular-array-loop/)

### 4. Pattern: Merge Intervals

1. Introduction [Educative.io](https://www.educative.io/courses/grokking-the-coding-interview/3YVYvogqXpA)
2. Merge Intervals [Educative.io](https://www.educative.io/courses/grokking-the-coding-interview/3jyVPKRA8yx)
3. Insert Interval [Educative.io](https://www.educative.io/courses/grokking-the-coding-interview/3jKlyNMJPEM)
4. Intervals Intersection [Educative.io](https://www.educative.io/courses/grokking-the-coding-interview/JExVVqRAN9D)
5. Conflicting Appointments [Geeksforgeeks](https://www.geeksforgeeks.org/check-if-any-two-intervals-overlap-among-a-given-set-of-intervals/)
6. Problem Challenge 1 - Minimum Meeting Rooms  [Lintcode](https://www.lintcode.com/problem/meeting-rooms-ii/)
7. Problem Challenge 2 - Maximum CPU Load  [Geeksforgeeks](https://www.geeksforgeeks.org/maximum-cpu-load-from-the-given-list-of-jobs/)
8. Problem Challenge 3 - Employee Free Time  [CoderTrain](https://www.codertrain.co/employee-free-time)

### 5. Pattern: Cyclic Sort

1. Introduction [emre.me](https://emre.me/coding-patterns/cyclic-sort/)
2. Cyclic Sort  [Geeksforgeeks](https://www.geeksforgeeks.org/sort-an-array-which-contain-1-to-n-values-in-on-using-cycle-sort/)
3. Find the Missing Number  [Leetcode](https://leetcode.com/problems/missing-number/)
4. Find all Missing Numbers  [Leetcode](https://leetcode.com/problems/find-all-numbers-disappeared-in-an-array/)
5. Find the Duplicate Number  [Leetcode](https://leetcode.com/problems/find-the-duplicate-number/)
6. Find all Duplicate Numbers  [Leetcode](https://leetcode.com/problems/find-all-duplicates-in-an-array/)
7. Problem Challenge 1 - Find the Corrupt Pair  [TheCodingSimplified](https://thecodingsimplified.com/find-currupt-pair/)
8. Problem Challenge 2 - Find the Smallest Missing Positive Number [Leetcode](https://leetcode.com/problems/first-missing-positive/)
9. Problem Challenge 3 - Find the First K Missing Positive Numbers  [TheCodingSimplified](https://thecodingsimplified.com/find-the-first-k-missing-positive-number/)

### 6. Pattern: In-place Reversal of a LinkedList

1. Introduction [emre.me](https://emre.me/coding-patterns/in-place-reversal-of-a-linked-list/)
2. Reverse a LinkedList  [Leetcode](https://leetcode.com/problems/reverse-linked-list/)
3. Reverse a Sub-list [Leetcode](https://leetcode.com/problems/reverse-linked-list-ii/)
4. Reverse every K-element Sub-list [Leetcode](https://leetcode.com/problems/reverse-nodes-in-k-group/)
5. Problem Challenge 1 - Reverse alternating K-element Sub-list [Geeksforgeeks](https://www.geeksforgeeks.org/reverse-alternate-k-nodes-in-a-singly-linked-list/)
6. Problem Challenge 2 - Rotate a LinkedList [Leetcode](https://leetcode.com/problems/rotate-list/)

### 7. Pattern: Tree Breadth First Search

1. Introduction
2. Binary Tree Level Order Traversal  [Leetcode](https://leetcode.com/problems/binary-tree-level-order-traversal/)
3. Reverse Level Order Traversal  [Leetcode](https://leetcode.com/problems/binary-tree-level-order-traversal-ii/)
4. Zigzag Traversal [Leetcode](https://leetcode.com/problems/binary-tree-zigzag-level-order-traversal/)
5. Level Averages in a Binary Tree  [Leetcode](https://leetcode.com/problems/average-of-levels-in-binary-tree/)
6. Minimum Depth of a Binary Tree  [Leetcode](https://leetcode.com/problems/minimum-depth-of-binary-tree/)
7. Maximum Depth of a Binary Tree  [Leetcode](https://leetcode.com/problems/maximum-depth-of-binary-tree/)
8. Level Order Successor  [Geeksforgeeks](https://www.geeksforgeeks.org/level-order-successor-of-a-node-in-binary-tree/)
9. Connect Level Order Siblings [Leetcode](https://leetcode.com/problems/populating-next-right-pointers-in-each-node/)
10. Problem Challenge 1 - Connect All Level Order Siblings [Educative](https://www.educative.io/m/connect-all-siblings)
11. Problem Challenge 2 - Right View of a Binary Tree  [Leetcode](https://leetcode.com/problems/binary-tree-right-side-view/)



### 8. Pattern: Tree Depth First Search

1. Introduction
2. Binary Tree Path Sum  [Leetcode](https://leetcode.com/problems/path-sum/)
3. All Paths for a Sum [Leetcode](https://leetcode.com/problems/path-sum-iii/)
4. Sum of Path Numbers [Leetcode](https://leetcode.com/problems/sum-root-to-leaf-numbers/)
5. Path With Given Sequence [Geeksforgeeks](https://www.geeksforgeeks.org/check-root-leaf-path-given-sequence/)
6. Count Paths for a Sum [Leetcode](https://leetcode.com/problems/path-sum-iii/)
7. Problem Challenge 1 - Tree Diameter [Leetcode](https://leetcode.com/problems/diameter-of-binary-tree/)
8. Problem Challenge 2 - Path with Maximum Sum  [Leetcode](https://leetcode.com/problems/binary-tree-maximum-path-sum/)

### 9. Pattern: Two Heaps

1. Introduction
2. Find the Median of a Number Stream [Leetcode](https://leetcode.com/problems/find-median-from-data-stream/)
3. Sliding Window Median 
4. Maximize Capital 

### 10. Pattern: Subsets

1. Introduction [Educative.io](https://www.educative.io/courses/grokking-the-coding-interview/R87WmWYrELz)
2. Subsets  [Educative.io](https://www.educative.io/courses/grokking-the-coding-interview/gx2OqlvEnWG)
3. Subsets With Duplicates  [Educative.io](https://www.educative.io/courses/grokking-the-coding-interview/7npk3V3JQNr)
4. Permutations [Educative.io](https://www.educative.io/courses/grokking-the-coding-interview/B8R83jyN3KY)
5. String Permutations by changing case 
6. Balanced Parentheses 
7. Unique Generalized Abbreviations 

### 11. Pattern: Modified Binary Search

1. Introduction
2. Order-agnostic Binary Search  [Geeksforgeeks](https://www.geeksforgeeks.org/order-agnostic-binary-search/)  
3. Ceiling of a Number [Geeksforgeeks-Ceil](https://www.geeksforgeeks.org/ceiling-in-a-sorted-array/) [Geeksforgeeks-Floor](https://www.geeksforgeeks.org/floor-in-a-sorted-array/)  
4. Next Letter [Leetcode](https://leetcode.com/problems/find-smallest-letter-greater-than-target/)  
5. Number Range [Leetcode](https://leetcode.com/problems/find-first-and-last-position-of-element-in-sorted-array/)  
6. Search in a Sorted Infinite Array [Leetcode](https://www.geeksforgeeks.org/find-position-element-sorted-array-infinite-numbers/)  
7. Minimum Difference Element - Find the floor & ceil take the difference, minimum would be the ans
8. Bitonic Array Maximum  [Geeksforgeeks](https://www.geeksforgeeks.org/find-the-maximum-element-in-an-array-which-is-first-increasing-and-then-decreasing/)
9. Problem Challenge 1 - Search Bitonic Array [Leetcode](https://leetcode.com/problems/find-in-mountain-array/)  
10. Problem Challenge 2 - Search in Rotated Array [Leetcode](https://leetcode.com/problems/search-in-rotated-sorted-array/)
11. Problem Challenge 3 - Rotation Count [Geeksforgeeks](https://www.geeksforgeeks.org/find-rotation-count-rotated-sorted-array/)  

### 12. Pattern: Bitwise XOR

1. Introduction
2. Single Number 
3. Two Single Numbers 
4. Complement of Base 10 Number 

### 13. Pattern: Top 'K' Elements

1. Introduction
2. Top 'K' Numbers 
3. Kth Smallest Number 
4. 'K' Closest Points to the Origin  [Leetcode](https://leetcode.com/problems/k-closest-points-to-origin/)
5. Connect Ropes 
6. Top 'K' Frequent Numbers 
7. Frequency Sort 
8. Kth Largest Number in a Stream [Leetcode](https://leetcode.com/problems/kth-largest-element-in-a-stream/)
9. 'K' Closest Numbers 
10. Maximum Distinct Elements 
11. Sum of Elements 
12. Rearrange String 

### 14. Pattern: K-way merge

1. Introduction
2. Merge K Sorted Lists 
3. Kth Smallest Number in M Sorted Lists (Medium)
4. Kth Smallest Number in a Sorted Matrix (Hard) [Educative.io](https://www.educative.io/courses/grokking-the-coding-interview/x1NJVYKNvqz)
5. Smallest Number Range (Hard)

### 15. Pattern : 0/1 Knapsack (Dynamic Programming)

1. Introduction
2. 0/1 Knapsack [Geeksforgeeks](https://www.geeksforgeeks.org/0-1-knapsack-problem-dp-10/)
3. Equal Subset Sum Partition [Leetcode](https://leetcode.com/problems/partition-equal-subset-sum/)
4. Subset Sum [Geeksforgeeks](https://www.geeksforgeeks.org/subset-sum-problem-dp-25/)
5. Minimum Subset Sum Difference  [Geeksforgeeks](https://www.geeksforgeeks.org/partition-a-set-into-two-subsets-such-that-the-difference-of-subset-sums-is-minimum/)

### 16. Pattern: Topological Sort (Graph)

1. Introduction
2. Topological Sort [Youtube](https://www.youtube.com/watch?v=cIBFEhD77b4)
3. Tasks Scheduling [Leetcode-Similar](https://leetcode.com/problems/course-schedule/)
4. Tasks Scheduling Order [Leetcode-Similar](https://leetcode.com/problems/course-schedule/)
5. All Tasks Scheduling Orders  [Leetcode-Similar](https://leetcode.com/problems/course-schedule-ii/)
6. Alien Dictionary  [Leetcode](https://leetcode.com/problems/alien-dictionary/)
7. Problem Challenge 1 - Reconstructing a Sequence  [Leetcode](https://leetcode.com/problems/sequence-reconstruction/)
8. Problem Challenge 2 - Minimum Height Trees  [Leetcode](https://leetcode.com/problems/minimum-height-trees/)

### 17. Miscellaneous

1. Kth Smallest Number
